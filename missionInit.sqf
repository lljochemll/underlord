//In this file you can edit all the parameters for the mission
//Not added: classnames for vehicle crews

//Variables//

Zen_Print_All_Errors = true; 

//Classnames for units
enemyPool = ["rhs_msv_rifleman","rhs_msv_LAT","rhs_msv_RShG2","rhs_msv_rifleman","rhs_msv_medic","rhs_msv_machinegunner_assistant","rhs_msv_machinegunner","rhs_msv_junior_sergeant","rhs_msv_strelok_rpg_assist","rhs_msv_at","rhs_msv_grenadier"];
enemyPoolCar = ["rhs_tigr_msv","rhs_tigr_ffv_msv","RHS_UAZ_MSV_01"];
enemyPoolTruck = ["rhs_gaz66_msv","rhs_gaz66o_msv","RHS_Ural_MSV_01","RHS_Ural_Open_MSV_01"];
enemyPoolApc = ["rhs_btr80a_msv","rhs_btr80_msv"];
enemyPoolIfv = ["rhs_prp3_msv","rhs_brm1k_msv","rhs_bmp3_late_msv","rhs_bmd4m_vdv","rhs_bmd2m"];
enemyPoolTank = ["rhs_t80um"];
enemyPoolTransportheli = ["RHS_Mi8AMT_vvs"];
enemyPoolAttackheli = ["RHS_Ka52_vvs","RHS_Mi24P_CAS_vvs"];
enemyPoolLogistics = ["RHS_Ural_Fuel_MSV_01","rhs_gaz66_repair_msv","rhs_gaz66_ap2_msv","rhs_gaz66_r142_msv"];
enemyPoolSF = ["y_t_recon_tl","y_t_recon","y_t_recon_at","y_t_recon_m","y_t_recon_medic"];
enemyOfficer = "O_officer_F";

//Other
occupiedTowns = ["Telos","Gravia","Kalithea","Sagonisi","Ekali"];	//Use this array to blacklist locations

occupiedTownsLoc = [];	//Init, don't change this
occupiedCount= 0;		//Init, don't change this
runningTasks = [];	//Init, don't change this

["mrk_safeZone",east,west] call Zen_ShowHideMarkers;

hint "Init complete";

[]execVM "Tasks\taskManager.sqf";