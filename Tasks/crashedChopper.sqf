//Set up var
_name = [5] call Zen_StringGenerateRandom;
rescued = 0;
pilotDead = 0;


//Get position
 _location = [0,0,["mrk_safeZone"],1,0,0,[1,0,10],0,0,[1,5,5],[1,[0,0,0],10]] call Zen_FindGroundPosition;
 _dropLocation = getMarkerPos "mrk_return";
 

//Spawn chopper + crew
_chopper = [_location, "Land_Wreck_Heli_Attack_01_F"] call Zen_SpawnVehicle;

_location= [_location, random 20, random 20] call BIS_fnc_relPos;
_groupPilot = createGroup west;
pilot = _groupPilot createUnit ["B_Helipilot_F",_location, [], 0, "NONE"];

pilot setVariable ["AGM_AllowUnconscious", true]; 
[pilot, 999999] call AGM_Medical_fnc_knockOut; 
pilot setDamage 0.5;
[pilot, true] call AGM_Drag_fnc_makeDraggable;
//_pilot switchMove "acts_InjuredAngryRifle01";	AGM overrides this (looking to fix this as I find this animation better)


//Create task markers

_location= [_location, random 150, random 150] call BIS_fnc_relPos;
_marker = createMarker [_name, _location];
_name setMarkerShape "RECTANGLE";
_name setMarkerSize [200,200];
_name setMarkerBrush "Solid";
_name setMarkerColor "ColorBLUFOR";


//Create task
_task = [west,"An experimental Comanche helicopter was shot down by OPFOR forces and made an emergency landing. Satellites spotted enemy forces assembling at the airfield witch are due to leave for the crash site. 
Secure the crash site and extract the pilot","Secure downed Comanche",_location,false,"",_name] call Zen_InvokeTask;


_x = 0;

//Spawn enemy's
_groupCount = 7;
_carCount = 4;

//infantry
while{_x < _groupCount} do{
	_locationS = [_location, random 300, random 300] call BIS_fnc_relPos;
	_locationS = [_locationS,1,1] call Zen_ExtendPosition;
	_group = [_locationS, east, "infantry", 4,"Basic"] call Zen_SpawnInfantry;
	[_group,_location,[0,300],[0,360],"limited","safe"] spawn Zen_OrderInfantryPatrol;
	_x = _x + 1;
};

_x = 0;

//cars
while{_x < _carCount} do{
	_locationS = [_location, random 300, random 300] call BIS_fnc_relPos;
	_locationS = [_locationS,0,0,1,[1,700]] call Zen_FindGroundPosition;
	_veh = [_locationS,(enemyPoolCar call BIS_fnc_selectRandom)] call Zen_SpawnVehicle;
	[_veh,east] call Zen_SpawnVehicleCrew; 
	_x = _x + 1;
};

_x = 0;


//Spawn complete trigger
_trgdrop = createTrigger ["EmptyDetector",_dropLocation];
_trgdrop setTriggerArea [10,10,10,false];
_trgdrop setTriggerActivation ["BLUFOR","PRESENT", false];
_trgdrop setTriggerStatements ["{[thisTrigger, _x]call BIS_fnc_inTrigger}count [pilot, blu_hostage_2] > 0","rescued = 1",""];

//Spawn dead trigger
_trgdead = createTrigger ["EmptyDetector",_dropLocation];
_trgdead setTriggerStatements ["!alive pilot","pilotDead = 1",""];

waitUntil { rescued == 1 or pilotDead == 1};


//Complete task
if(rescued == 1) then{
	[_name, "succeeded"] call Zen_UpdateTask;
};

if(pilotDead == 1) then{
	[_name, "failed"] call Zen_UpdateTask;
};

Sleep 5;

taskActive = 0;


//Cleanup
deleteMarker _name;