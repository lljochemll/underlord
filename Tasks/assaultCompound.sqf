//var
_name = [5] call Zen_StringGenerateRandom;


//location
_markers = ["mrk_task_compound_0","mrk_task_compound_1","mrk_task_compound_2","mrk_task_compound_3","mrk_task_compound_4","mrk_task_compound_5","mrk_task_compound_6","mrk_task_compound_7","mrk_task_compound_8"];
_marker = _markers call BIS_fnc_selectRandom;

_location = getMarkerPos _marker;


//Create task
_task = [west,"Told you I haven't finished everything","Assault compound",_location,true,"",_name] call Zen_InvokeTask;

_x = 0;

//Spawn enemy's
_groupCount = 5;
_garrisonCount = 5;
_carCount = 5;
_lightCount = 2;
_tankCount = 1;

//infantry
while{_x < _groupCount} do{
	_locationS = [_location, random 200, random 200] call BIS_fnc_relPos;
	_locationS = [_locationS,1,1] call Zen_ExtendPosition;
	_group = [_locationS, east, "infantry", 4,"Basic"] call Zen_SpawnInfantry;
	[_group,_location,[0,300],[0,360],"limited","safe"] spawn Zen_OrderInfantryPatrol;
	_x = _x + 1;
};

_x = 0;
_locationS = _location;

//garrisoned infantry
while{_x < _garrisonCount} do{
	_group = [_locationS, east, "infantry", 4,"Basic"] call Zen_SpawnInfantryGarrison;
	_locationS = [_location, random 100, random 100] call BIS_fnc_relPos;
	_locationS = [_locationS,1,1] call Zen_ExtendPosition;
	_x = _x + 1;
};

_x = 0;

//APC
while{_x < _lightCount} do{
	_locationS = [_location, random 300, random 300] call BIS_fnc_relPos;
	_locationS = [_locationS,0,0,1,[1,700]] call Zen_FindGroundPosition;
	_veh = [_locationS,(enemyPoolApc call BIS_fnc_selectRandom)] call Zen_SpawnVehicle;
	[_veh,east] call Zen_SpawnVehicleCrew; 
	_x = _x + 1;
};

_x = 0;

//IFV
while{_x < _lightCount} do{
	_locationS = [_location, random 300, random 300] call BIS_fnc_relPos;
	_locationS = [_locationS,0,0,1,[1,700]] call Zen_FindGroundPosition;
	_veh = [_locationS,(enemyPoolIfv call BIS_fnc_selectRandom)] call Zen_SpawnVehicle;
	[_veh,east] call Zen_SpawnVehicleCrew; 
	_x = _x + 1;
};

_x = 1;

//Spawn complete trigger
_trg = createTrigger ["EmptyDetector",_location];
_trg setTriggerArea [150,150,0,false];
_trg setTriggerActivation ["EAST","PRESENT", true];
_trg setTriggerStatements ["this","",""];


//Wait until all enemies dead or out of town
waitUntil{sleep 5; count list _trg < 2};

[_name, "succeeded"] call Zen_UpdateTask;
taskActive = 0;
