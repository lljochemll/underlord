//var
_name = [5] call Zen_StringGenerateRandom;
captured = 0;
dead = 0;
_ok = 0;


//Set location for triggers
_missionDropoffzone = getMarkerPos "mrk_return";


//location
_types = ["compound","town"];
_type = _types call BIS_fnc_selectRandom;

if(_type == "compound")then{
	_markers = ["mrk_task_compound_0","mrk_task_compound_1","mrk_task_compound_2","mrk_task_compound_3","mrk_task_compound_4","mrk_task_compound_5","mrk_task_compound_6","mrk_task_compound_7","mrk_task_compound_8"];
	_marker = _markers call BIS_fnc_selectRandom;
	locationC = getMarkerPos _marker;
};

if(_type == "town")then{
	towns = nearestLocations [getPosATL player, ["NameCity","NameCityCapital","NameVillage"], 25000];
		while{_ok == 0}do{
			targetTown = towns select (floor (random (count towns)));
			townName = text targetTown;
			if (!(townName in occupiedTowns))then{_ok = 1;};
			Sleep 1;
		};
	locationC = position (targetTown); 
};


//Create task
_task = [west,"Intel shows the location of an enemy officer. Move in, execute capture order and extract hime to base.","Capture officer",locationC,false,"",_name] call Zen_InvokeTask;


//Spawn enemies
_location = locationC;

_groupCount = 5;
_garrisonCount = 7; 
_carCount = 3;
_lightCount = 1;
_tankCount = 0;

//infantry
while{_x < _groupCount} do{
	_locationS = [_location, random 300, random 300] call BIS_fnc_relPos;
	_locationS = [_locationS,1,1] call Zen_ExtendPosition;
	_group = [_locationS, east, "infantry", 5,"Basic"] call Zen_SpawnInfantry;
	[_group,_location,[0,300],[0,360],"limited","safe"] spawn Zen_OrderInfantryPatrol;
	_x = _x + 1;
};

_x = 0;

//garrisoned infantry
while{_x < _garrisonCount} do{
	_locationS = [_location, random 300, random 300] call BIS_fnc_relPos;
	_locationS = [_locationS,1,1] call Zen_ExtendPosition;
	_group = [_locationS, east, "infantry", 2,"Basic"] call Zen_SpawnInfantryGarrison;
	_x = _x + 1;
};

_x = 0;

//cars
while{_x < _carCount} do{
	_locationS = [_location, random 300, random 300] call BIS_fnc_relPos;
	_locationS = [_locationS,0,0,1,[1,700]] call Zen_FindGroundPosition;
	_veh = [_locationS,(enemyPoolCar call BIS_fnc_selectRandom)] call Zen_SpawnVehicle;
	[_veh,east] call Zen_SpawnVehicleCrew; 
	_x = _x + 1;
};

_x = 0;

//APC
while{_x < _lightCount} do{
	_locationS = [_location, random 300, random 300] call BIS_fnc_relPos;
	_locationS = [_locationS,0,0,1,[1,700]] call Zen_FindGroundPosition;
	_veh = [_locationS,(enemyPoolApc call BIS_fnc_selectRandom)] call Zen_SpawnVehicle;
	[_veh,east] call Zen_SpawnVehicleCrew; 
	_x = _x + 1;
};

_x = 0;

//IFV
while{_x < _lightCount} do{
	_locationS = [_location, random 300, random 300] call BIS_fnc_relPos;
	_locationS = [_locationS,0,0,1,[1,700]] call Zen_FindGroundPosition;
	_veh = [_locationS,(enemyPoolIfv call BIS_fnc_selectRandom)] call Zen_SpawnVehicle;
	[_veh,east] call Zen_SpawnVehicleCrew; 
	_x = _x + 1;
};

_x = 0;


//Spawn officer
_locationS = [_location, random 150, random 150] call BIS_fnc_relPos;

_groupOfficer = createGroup east;
officer = _groupOfficer createUnit [enemyOfficer,_locationS, [], 0, "NONE"];
officer allowFleeing 0;
_locationOfficer = [officer] call Zen_FindBuildingPositions;
_locationOfficer = _locationOfficer call BIS_fnc_selectRandom;
officer setPos _locationOfficer;


//Spawn dropoff trigger
_trgdrop = createTrigger ["EmptyDetector",_missionDropoffzone];
_trgdrop setTriggerArea [20,20,20,false];
_trgdrop setTriggerActivation ["officer","PRESENT", false];
_trgdrop setTriggerStatements ["officer distance thistrigger < 10","captured = 1",""];

//Spawn dead trigger
_trgdead = createTrigger ["EmptyDetector",_missionDropoffzone];
_trgdead setTriggerStatements ["!alive officer","dead = 1",""];


//Wait until complete or failed
waitUntil {dead == 1 or captured == 1};

if ( dead == 1) then{
	[_name, "failed"] call Zen_UpdateTask;
};

if ( captured== 1) then{
	[_name, "succeeded"] call Zen_UpdateTask;
};
sleep 15;


//Cleanup
deleteVehicle officer;
taskActive = 0;