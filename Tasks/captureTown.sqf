_ok = 0;
_name = [5] call Zen_StringGenerateRandom;

//Get location of town
towns = nearestLocations [getPosATL player, ["NameCity","NameCityCapital","NameVillage"], 25000];

while{_ok == 0}do{
	targetTown = towns select (floor (random (count towns)));
	townName = text targetTown;
	if (!(townName in occupiedTowns))then{_ok = 1;};
	Sleep 1;
};

_location = position (targetTown); 

//Create task
_text  = format ["Russian forces have captured %1. We need to retake that town ASAP!", townName];
_task = [west,_text,"Retake town",_location,true,"",_name] call Zen_InvokeTask;


//Create marker
_name = [5] call Zen_StringGenerateRandom;

_marker = createMarker [_name, _location];
_name setMarkerShape "RECTANGLE";
_name setMarkerSize [350,350];
_name setMarkerBrush "Border";
_name setMarkerColor "ColorOPFOR";


_x = 0;

//Spawn enemy's
_groupCount = 7;
_garrisonCount = 10; 
_carCount = 5;
_lightCount = 2;
_tankCount = 1;

//infantry
while{_x < _groupCount} do{
	_locationS = [_location, random 300, random 300] call BIS_fnc_relPos;
	_locationS = [_locationS,1,1] call Zen_ExtendPosition;
	_group = [_locationS, east, "infantry", 5,"Basic"] call Zen_SpawnInfantry;
	[_group,_location,[0,300],[0,360],"limited","safe"] spawn Zen_OrderInfantryPatrol;
	_x = _x + 1;
};

_x = 0;

//garrisoned infantry
while{_x < _garrisonCount} do{
	_locationS = [_location, random 300, random 300] call BIS_fnc_relPos;
	_locationS = [_locationS,1,1] call Zen_ExtendPosition;
	_group = [_locationS, east, "infantry", 2,"Basic"] call Zen_SpawnInfantryGarrison;
	_x = _x + 1;
};

_x = 0;

//cars
while{_x < _carCount} do{
	_locationS = [_location, random 300, random 300] call BIS_fnc_relPos;
	_locationS = [_locationS,0,0,1,[1,700]] call Zen_FindGroundPosition;
	_veh = [_locationS,(enemyPoolCar call BIS_fnc_selectRandom)] call Zen_SpawnVehicle;
	[_veh,east] call Zen_SpawnVehicleCrew; 
	_x = _x + 1;
};

_x = 0;

//APC
while{_x < _lightCount} do{
	_locationS = [_location, random 300, random 300] call BIS_fnc_relPos;
	_locationS = [_locationS,0,0,1,[1,700]] call Zen_FindGroundPosition;
	_veh = [_locationS,(enemyPoolApc call BIS_fnc_selectRandom)] call Zen_SpawnVehicle;
	[_veh,east] call Zen_SpawnVehicleCrew; 
	_x = _x + 1;
};

_x = 0;

//IFV
while{_x < _lightCount} do{
	_locationS = [_location, random 300, random 300] call BIS_fnc_relPos;
	_locationS = [_locationS,0,0,1,[1,700]] call Zen_FindGroundPosition;
	_veh = [_locationS,(enemyPoolIfv call BIS_fnc_selectRandom)] call Zen_SpawnVehicle;
	[_veh,east] call Zen_SpawnVehicleCrew; 
	_x = _x + 1;
};

_x = 0;

//tanks
while{_x < _tankCount} do{
	_locationS = [_location, random 300, random 300] call BIS_fnc_relPos;
	_locationS = [_locationS,1,1] call Zen_ExtendPosition;
	_locationS = [_locationS,0,0,1,[1,700]] call Zen_FindGroundPosition;
	_veh = [_locationS,(enemyPoolTank call BIS_fnc_selectRandom)] call Zen_SpawnVehicle;
	[_veh,east] call Zen_SpawnVehicleCrew; 
	_x = _x + 1;
};


//Spawn complete trigger
_trg = createTrigger ["EmptyDetector",_location];
_trg setTriggerArea [350,350,0,false];
_trg setTriggerActivation ["EAST","PRESENT", true];
_trg setTriggerStatements ["this","",""];


//Wait until all enemies dead or out of town
waitUntil{sleep 5; count list _trg < 2};

[_name, "succeeded"] call Zen_UpdateTask;
taskActive = 0;

deleteMarker _marker;