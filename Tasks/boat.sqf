//var
_name = [5] call Zen_StringGenerateRandom;
secured = 0;
destroyed = 0;


//Set location for triggers
_missionDropoffzone = getMarkerPos "mrk_return";


//location
_location = [0,0,[],2] call Zen_FindGroundPosition;


//Spawn cache and wreck
cache = "Box_IND_AmmoVeh_F" createVehicle _location;
_locationW = [_location,10,10] call Zen_ExtendPosition;
_wreck = "Land_Wreck_Plane_Transport_01_F" createVehicle ([_locationW, random 50, random 50] call BIS_fnc_relPos);
[cache, true] call AGM_Drag_fnc_makeDraggable;


//Create task
_task = [west,"A C-130 was shot down overseas and dropped an experimental weapons cache. Dive down to the wreckage and return the cache to base","Return sunken cache",_location,false,"",_name] call Zen_InvokeTask;


//Spawn enemies
_x = 0;
_boatCount = 5;
_helicopterCount = 3;

//Boats
while{_x < _boatCount} do{
	_locationS = [_location, random 300, random 300] call BIS_fnc_relPos;
	_boat = [_locationS,"O_Boat_Armed_01_hmg_F"] call Zen_SpawnBoat;
	[_boat, _location, 500] spawn Zen_OrderBoatPatrol;
	_x = _x +1;
};

_x = 0;

//Helicopters
while{_x < _helicopterCount} do{
	_locationS = [_location, random 500, random 500] call BIS_fnc_relPos;
	_heliA = [_locationS,enemyPoolAttackheli,50] call Zen_SpawnHelicopter;
	_x = _x + 1;
	
};

//Spawn dropoff trigger
_trgdrop = createTrigger ["EmptyDetector",_missionDropoffzone];
_trgdrop setTriggerArea [20,20,20,false];
_trgdrop setTriggerActivation ["cahce","PRESENT", false];
_trgdrop setTriggerStatements ["cache distance thistrigger < 10","secured = 1",""];


//Spawn dead trigger
_trgdead = createTrigger ["EmptyDetector",_missionDropoffzone];
_trgdead setTriggerStatements ["!alive cache","destroyed = 1",""];


//Wait until complete or failed
waitUntil {destroyed == 1 or secured == 1};

if ( destroyed == 1) then{
	[_name, "failed"] call Zen_UpdateTask;
};

if ( secured == 1) then{
	[_name, "succeeded"] call Zen_UpdateTask;
};
sleep 15;


//Cleanup
deleteVehicle cache;
deleteVehicle _wreck;
taskActive = 0;