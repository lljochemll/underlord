Sleep 60;


taskActive = 0;
ok = 0;

assault = 0;
boat = 0;
captureT = 0;
captureV = 0;
convoy = 0;
crash = 0;
defend = 0;
deliver = 0;
meeting = 0;
radar = 0;
radiotower = 0;
rescue = 0;
uav = 0;

while{true}do{
	_tasks = ["assault","boat","capture","captureT","crash","rescue"];
	_task = _tasks call BIS_fnc_selectRandom;
	
	if(_task == "assault") then{
		[]execVM "Tasks\assaultCompound.sqf";
		taskActive = 1;
	};
	if(_task == "boat") then{
		[]execVM "Tasks\boat.sqf";
		taskActive = 1;
	};
	if(_task == "capture") then{
		[]execVM "Tasks\capture.sqf";
		taskActive = 1;
	};
	if(_task == "captureT") then{
		[]execVM "Tasks\captureTown.sqf";
		taskActive = 1;
	};
	if(_task == "crash") then{
		[]execVM "Tasks\crashedChopper.sqf";
		taskActive = 1;
	};
	if(_task == "rescue") then{
		[]execVM "Tasks\rescue.sqf";
		taskActive = 1;
	};
	
	waitUntil{taskActive == 0;};
	Sleep 60;
};