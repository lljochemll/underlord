//Set up var
rescued = 0;
dead = 0;
_x = 0;
_y = 0;
_ok = 0;
_name = [5] call Zen_StringGenerateRandom;


//Get location of town
towns = nearestLocations [getPosATL player, ["NameCity","NameCityCapital","NameVillage"], 25000];

while{_ok == 0}do{
	targetTown = towns select (floor (random (count towns)));
	townName = text targetTown;
	if (!(townName in occupiedTowns))then{_ok = 1;};
	Sleep 1;
};

_location = position (targetTown);


//Set location for triggers
_missionDropoffzone = getMarkerPos "mrk_return";


//Create task
_task = [west,"A journalist has been taken hostage by enemy SOF. Attempts to negotiate with them 
have unfortunately failed and the Russians are planning on executing him. This could have a big influence 
on public opinions, something we can't afford at all. Once you've located him, rescue him and return 
him to the Return point","Rescue Journalist",_location,true,"",_name] call Zen_InvokeTask;


//Spawn hostage
_locationS = [_location, random 150, random 150] call BIS_fnc_relPos;

_groupHostage = createGroup civilian;
CIV = _groupHostage createUnit ["C_journalist_F",_locationS, [], 0, "NONE"];
CIV allowFleeing 0;
_locationHostage = [CIV] call Zen_FindBuildingPositions;
_locationHostage = _locationHostage call BIS_fnc_selectRandom;
CIV setPos _locationHostage;

//Hostage set behaviour
CIV setBehaviour "Careless";
doStop CIV;
commandStop CIV;
CIV switchMove "AmovPercMstpSsurWnonDnon";
[CIV, true, true] call AGM_Interaction_fnc_setCaptive;


//Spawn enemy
_groupCount = 6;

[_locationS,east, "sof", [4,6],"SF"] call Zen_SpawnInfantryGarrison;

while{_x < _groupCount} do{
	_locationS = [_location, random 300, random 300] call BIS_fnc_relPos;
	_locationS = [_locationS,1,1] call Zen_ExtendPosition;
	_group = [_locationS,east, "sof", [4,6],"SF"] call Zen_SpawnInfantry;
	[_group,_location,[0,300],[0,360],"limited","safe"] spawn Zen_OrderInfantryPatrol;
	_x = _x + 1;
};



//Spawn dropoff trigger
_trgdrop = createTrigger ["EmptyDetector",_missionDropoffzone];
_trgdrop setTriggerArea [50,50,50,false];
_trgdrop setTriggerActivation ["CIV","PRESENT", false];
_trgdrop setTriggerStatements ["{[thisTrigger, _x]call BIS_fnc_inTrigger}count [CIV, blu_hostage_2] > 0","rescued = 1",""];


//Spawn dead trigger
_trgdead = createTrigger ["EmptyDetector",_missionDropoffzone];
_trgdead setTriggerStatements ["!alive CIV","dead = 1",""];


//Wait until complete or failed
waitUntil {dead == 1 or rescued == 1};

if ( dead == 1) then{
	[_name, "failed"] call Zen_UpdateTask;
};

if ( rescued == 1) then{
	[_name, "succeeded"] call Zen_UpdateTask;
};
sleep 15;


//Cleanup
deleteVehicle CIV;
taskActive = 0;